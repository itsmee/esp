#include <OneWire.h>			// DS18
#include <SoftwareSerial.h>
#include <PubSubClient.h>		// MQTT (open the Arduino IDE, in the menu click on sketch -> include library -> manage libraries and install "PubSubClient by Nick �O Leary"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

//*******************************************************************
//
// ESP8266 sketch that'll read from sensors (e.g. temp from DS18) 
// and deliver results to an MQTT queue
// 
// 
const String VERSION = "D-send2mqtt.ino 2016-12-29";
//
// ToDo
//  - Deepsleep?
//  - Sensoren?
//  - WebServer?
//
//*******************************************************************

//*******************************************************************
// getESPid
//*******************************************************************
String macToID(const uint8_t* mac) {
  String result;
  for (int i = 3; i < 6; ++i) {
    result += String(mac[i], 16);
  }
  result.toUpperCase();
  return result;
}

String getESPid(void) {
  String id, espid;
  uint8_t mac[6];

  WiFi.macAddress(mac);
  id = macToID(mac);
  espid = "ESP_" + id;

  return espid;
}

//*******************************************************************
// Variablen
//*******************************************************************

// loop 
unsigned long millisPrev=300000;

// WIFI
const char* ssid     = "SSID";
const char* password = "PWD";

// OneWire
const int pinLED = 14;
const int pinTaster = 4;
const int pinDS18 = 13;
const int pinPIR = 12;
OneWire  ds(pinDS18);  // a 4.7K resistor is necessary

// Taster
// ESP-01 GPIO Mapping   [0]=3,[2]=4,[4]=2,[5]=1,[12]=6,[13]=7,[14]=5,[15]=8,[16]=0
int tasterStatus = 0;
int tasterStatusLast = 0;

// PIR
int pirStatus=0;
int pirStatusLast=0;

// MQTT
#define mqtt_server "mqttserver.de"
#define mqtt_user "MQTTUSER"
#define mqtt_password "PWD"

String hw_info_topic     = String("sensor/") + getESPid() + String("/info");
String humidity_topic    = String("sensor/") + getESPid() + String("/humidity");
String temperature_topic = String("sensor/") + getESPid() + String("/temperature");

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);


//*******************************************************************
// Function: readTemp
//*******************************************************************
float readTemp() {
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;

  if ( !ds.search(addr)) {
    ds.reset_search();
    delay(500);
    if ( !ds.search(addr)) {
      ds.reset_search();
      delay(500);
      return (float) - 1;
    }
  }

  Serial.print("ROM =");
  for ( i = 0; i < 8; i++) {
    Serial.write(' ');
    Serial.print(addr[i], HEX);
  }

  if (OneWire::crc8(addr, 7) != addr[7]) {
    Serial.println("CRC is not valid!");
    return (float) - 2;
  }
  // Serial.println();

  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      // Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      // Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      // Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      Serial.println("Device is not a DS18x20 family device.");
      return (float) - 3;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end

  delay(1000);        // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  // Serial.print("  Data = ");
  // Serial.print(present, HEX);
  // Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  // Serial.print(" CRC=");
  // Serial.print(OneWire::crc8(data, 8), HEX);
  // Serial.println();

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  Serial.print("  Temperature = ");
  Serial.print(celsius);
  Serial.print(" Celsius");
  // Serial.print(fahrenheit);
  // Serial.println(" Fahrenheit");

  return celsius;
}

//*******************************************************************
// MQTT reconnect
//*******************************************************************
void mqtt_reconnect() {
  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (mqttClient.connect("ESP8266Client")) {
    // if (mqttClient.connect(getESPid(), mqtt_user, mqtt_password)) {
    if (mqttClient.connect(getESPid().c_str(), mqtt_user, mqtt_password)) {      
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


//*******************************************************************
// Setup
//*******************************************************************
void setup(void) {
  Serial.begin(9600);

  //****************************************************
  // Version
  //****************************************************
  Serial.println(VERSION);

  //****************************************************
  // Wifi
  //****************************************************
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  //****************************************************
  // MQTT
  //****************************************************
  mqttClient.setServer(mqtt_server, 1883);
  
  if (!mqttClient.connected()) {
    mqtt_reconnect();
  }
  mqttClient.loop();

  mqttClient.publish(hw_info_topic.c_str(), VERSION.c_str(), true);

  IPAddress localAddr = WiFi.localIP(); byte oct1 = localAddr[0]; byte oct2 = localAddr[1]; byte oct3 = localAddr[2]; byte oct4 = localAddr[3];  char s[16];  
  sprintf(s, "%d.%d.%d.%d", oct1, oct2, oct3, oct4);
  mqttClient.publish(hw_info_topic.c_str(), s, true);

  //****************************************************
  // MQTT
  //****************************************************
  pinMode(pinPIR,INPUT);
}


//****************************************************
// Loop
//****************************************************
void loop(void) {
  
  //****************************************************
  // trigger some every 5 minutes - other all the time
  //****************************************************
  if (millis() - millisPrev > 300000) {  
//    mqttClient.publish(temperature_topic.c_str(),String("Large").c_str(),true);
//    mqttClient.publish(temperature_topic.c_str(),String(millis()).c_str(),true);
//    mqttClient.publish(temperature_topic.c_str(),String(millisPrev).c_str(), true);
    millisPrev = millis();
    // Every x Minutes - transfer temp
  
    //****************************************************
    // Temp ausgeben
    //****************************************************
    if (!mqttClient.connected()) {
      mqtt_reconnect();
    }
    mqttClient.publish(temperature_topic.c_str(), String(readTemp()).c_str(), true);
  }
  else {
    // Small tasks - like motion detection
//    mqttClient.publish(temperature_topic.c_str(),String("Small").c_str(),true);
//    mqttClient.publish(temperature_topic.c_str(),String(millis()).c_str(),true);
//    mqttClient.publish(temperature_topic.c_str(),String(millisPrev).c_str(), true);
  
    //****************************************************
    // Tasterstatus abfragen
    //****************************************************
//    tasterStatus = digitalRead(pinTaster);

//    if ( tasterStatus != tasterStatusLast) {
//      Serial.print("tasterStatus"); Serial.println(tasterStatus);
//      if ( tasterStatus == 0) {
//        if (!mqttClient.connected()) {
//          mqtt_reconnect();
//        }
//        mqttClient.publish(hw_info_topic.c_str(), VERSION.c_str(), true);
//      }
//      tasterStatusLast = tasterStatus;
//      delay(1000);
//    }

    //****************************************************
    // PIR motion detection
    //****************************************************
    pirStatus = digitalRead(pinPIR);
    Serial.println(pirStatus);
    if (pirStatus == 1 && pirStatusLast!= 1) {    // Motion
      if (!mqttClient.connected()) {
        mqtt_reconnect();
      }
      mqttClient.publish("outTopic", "Motion Detected");  
      mqttClient.publish("outTopic", (String(pirStatus)+"-"+String(pirStatusLast)).c_str());  
      delay(1000);
    } else if (pirStatus == 0 && pirStatusLast!= 0) {    // Motion vorbei
      if (!mqttClient.connected()) {
        mqtt_reconnect();
      }
      mqttClient.publish("outTopic", (String(pirStatus)+"-"+String(pirStatusLast)).c_str());  
      delay(1000);
    }
    pirStatusLast = pirStatus;
  }

  //****************************************************
  // Sleep
  //****************************************************  
  delay(1000);
}
